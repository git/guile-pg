HACKING Guile-PG                                                -*- org -*-

This file is both a guide for newcomers and a todo list for oldstayers.
It lives in the repo but is not included in the distribution.

* other stuff to do
*** towards 1.0
***** (maybe) get buy-in for Guile bifurcation coping
***** (maybe) drop support for PostgreSQL 7.4
***** rename modules
      (database postgres) => (pg pq)
      (database postgres-foo) => (pg foo)
***** drop (pg table)
***** consolidate types / columns stuff
***** avoid keywords in (pg pq)
***** svelten symbols, make them Schemey (avoid underscore)
*** maintain test/OK as a set of related tables (like, you know, a database!)
*** fully [[file:src/libpq.org][support]] PostgreSQL 8.x, 9.x
*** add UPDATE (as comma-command ‘fup’) to gxrepl
*** comment code (audience: intermediate scheme programmer)
*** extend (database postgres-qcons) to handle subquery expressions
    Particularly: IN (scalar form) and NOT IN (scalar form).  It would
    be nice to handle the subquery forms of these operators as well, in
    which case, might as well handle: EXISTS, ANY/SOME, ALL.  Lastly,
    row-wise comparison may not be worth the hassle.
*** provide builtin converters for all PostgreSQL "native" types
*** regularize error handling
*** review examples in documentation
*** enable post-install maintenance of (database postgres-meta)
***** write/document proc that updates *class-defs*
***** also use it at "make" time
*** add to the manual a section giving an overview of using large-objects
    This must explain the ideas of OID's as references to objects.
    Also we need examples of using lo-streams.  Perhaps a scheme
    implementation of lo-export and lo-import and, just for fun, a
    demonstration of executing scheme code contained in a large
    object.  There should also be an example of updating a table with
    a large number of large objects.  Perhaps it could take a
    directory with lots of binary files in it and store each file in a
    large object in a table listing the names of the files.
*** incorporate scheme result-iterator/stream procs
    This should provide query-level functions like (pg:for-each) which
    iterates over a set of tuples returned from a query.  Also provide
    a streams interface so that (pg:stream-cdr) and (pg:stream-car)
    return tuples.
*** clean up port type-hackery in src/libpq.c
    The weirdness is probably unavoidably related to Guile's port
    implementation.  However, the least we can do is understand it
    better.

* portability (see [[file:test/OK][test/OK]])
*** PostgreSQL 12+ no longer support CREATE TABLE WITH OIDS
    affects [[file:test/basic.scm][test/basic.scm]] ‘make-table!’

* coding standards
*** C code: indent -nut
*** indent with emacs (don't like the result? fix emacs!)
*** (setq indent-tabs-mode nil)
*** (add-hook 'before-save-hook 'delete-trailing-whitespace)
*** everything UTF-8
*** ChangeLog
***** ttn-style: TITLE LF LF {BLURB LF LF}* BODY
***** exclude titles suffixed "; nfc." (no functional change)

* copyright update policy
*** individual files only on change
*** (if (< 2 (- END BEGIN)) RANGE INDIVIDUAL)

* repo branch discipline
*** no ‘master’ -- do not panic!
*** archive
    This branch is for what was already released.  It will never be rebased.
    Its tip will always have a commit title starting with "Release".
*** p -- "perhaps", "probably"
    This branch is where largely-stable changes intended for the next release
    live.  It's very unlikely to undergo rebase, but not entirely impossible.
    (Since Guile-PG is not so popular, we don't bother announcing it.)
*** q-TOPIC -- "questionably", "querulously"
    These are experimental, exploring the particular TOPIC.  They are intended
    to be ephemeral, undergoing rebase, amendment, and ultimately removal.
