# hv.sh
#
# Author: Thien-Thi Nguyen <ttn@gnuvola.org>
# License: Public Domain

me=`basename $0`

if [ x"$1" = x--help ] ; then
    sed '/^##/,/^##/!d;/^##/d;s/^# //g;s/^#$//g' $0
    exit 0
fi

if [ x"$1" = x--version ] ; then
    echo $me $version
    exit 0
fi

drat ()
{
    echo >&2 $me: ERROR: "$@"
    exit 1
}

# hv.sh ends here
