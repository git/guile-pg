#!/bin/sh
# Usage: sh -x autogen.sh

set -e

#############################################################################
# Guile-BAUX

guile-baux-tool snuggle h src/snuggle/
guile-baux-tool snuggle m4 build-aux/
guile-baux-tool import \
    re-prefixed-site-dirs \
    c2x \
    tsar \
    c-tsar \
    tsin \
    gen-scheme-wrapper \
    punify \
    sofix \
    uninstall-sofixed \
    gbaux-do

#############################################################################
# Autotools

autoreconf --verbose --force --install --symlink --warnings=all

# These override what ‘autoreconf --install’ creates.
# Another way is to use gnulib's config/srclist-update.
actually ()
{
    gnulib-tool --verbose --copy-file $1 $2
}
actually doc/INSTALL.UTF-8 INSTALL
actually doc/fdl.texi

# We aren't really interested in the backup files.
rm -f INSTALL~

######################################################################
# Done.

: Now run configure and make.
: You must pass the "--enable-maintainer-mode" option to configure.

# autogen.sh ends here
