/* lob-legacy.c

   Copyright (C) 2021 Thien-Thi Nguyen
   Portions Copyright (C) 1999, 2000 Ian Grant

   This file is part of Guile-PG.

   Guile-PG is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   Guile-PG is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Guile-PG.  If not, see <http://www.gnu.org/licenses/>.  */

static unsigned long int lobp_tag;

#define LOB_BUFLEN 512

#define MAX_LOB_WRITE 7000

#define LOBPORTP(x)  (SCM_NIMP (x) && lobp_tag == SCM_TYP16 (x))

/* This actually appeared somewhere between Guile 1.6.0 and 1.6.8,
   but the exact release number is too much bother to dig up at the
   moment.  */
#if !GI_LEVEL_1_8
static SCM
scm_new_port_table_entry (long tag)
{
  SCM z;
  scm_port *pt;

  NEWCELL_X (z);
  SCM_SET_CELL_WORD_0 (z, tag);
  pt = scm_add_to_port_table (z);
  SCM_SETPTAB_ENTRY (z, pt);
  return z;
}
#endif


static SCM
lob_mklobport (SCM conn, Oid oid, int alod, long modes, const char *FUNC_NAME)
{
  SCM port = scm_new_port_table_entry (lobp_tag);
  lob_stream *lobp;
  scm_port *pt = SCM_PTAB_ENTRY (port);;

  lobp = GCMALLOC (sizeof (lob_stream), lob_name);

  NOINTS ();
  lobp->conn = conn;
  lobp->oid = oid;
  lobp->alod = alod;
  SCM_SET_CELL_WORD_0 (port, lobp_tag | modes);
  SCM_SETSTREAM (port, (SCM) lobp);

  /* Declare port random access.  */
  pt->rw_random = 1;

  /* Allocate buffers and set position/end pointers.  */
  if (SCM_INPUT_PORT_P (port))
    {
      if (! (pt->read_buf = malloc (LOB_BUFLEN)))
        MEMORY_ERROR ();
      pt->read_pos = pt->read_end = pt->read_buf;
      pt->read_buf_size = LOB_BUFLEN;
    }
  else
    {
      pt->read_pos
        = pt->read_buf
        = pt->read_end
        = &pt->shortbuf;
      pt->read_buf_size = 1;
    }
  if (SCM_OUTPUT_PORT_P (port))
    {
      if (! (pt->write_buf = malloc (LOB_BUFLEN)))
        MEMORY_ERROR ();
      pt->write_pos = pt->write_buf;
      pt->write_buf_size = LOB_BUFLEN;
    }
  else
    {
      pt->write_buf = pt->write_pos = &pt->shortbuf;
      pt->write_buf_size = 1;
    }
  pt->write_end = pt->write_buf + pt->write_buf_size;

  SCM_SET_CELL_WORD_0 (port, SCM_CELL_WORD_0 (port) & ~SCM_BUF0);

  INTSOK ();

  return port;
}

/* During lob_flush error, we decide whether to use SYSTEM_ERROR ("normal"
   error mechanism) or to write directly to stderr, depending on libguile's
   variable: scm_terminating.  If it's not available in some form (see
   guile-pg.m4 comments), we arrange to unconditionally write to stderr
   instead of risking further muck-up.  */
#if !HAVE_DECL_SCM_TERMINATING
# ifdef HAVE_LIBGUILE_TERMINATING
extern int terminating;
#   define scm_terminating terminating
# endif /* HAVE_LIBGUILE_TERMINATING */
#endif /* !HAVE_DECL_SCM_TERMINATING */

static void
lob_flush (SCM port)
{
#define FUNC_NAME __func__
  scm_port *pt = SCM_PTAB_ENTRY (port);
  lob_stream *lobp = LOB_STREAM (port);
  PGconn *conn = LOB_CONN (lobp);
  unsigned char *ptr = pt->write_buf;
  int init_size = pt->write_pos - pt->write_buf;
  int remaining = init_size;

  while (remaining > 0)
    {
      int count;
      NOINTS ();
      count = lo_write (conn, lobp->alod, (char *) ptr, remaining);
      INTSOK ();
      if (count < remaining)
        {
          /* Error.  Assume nothing was written this call, but
             fix up the buffer for any previous successful writes.  */
          int done = init_size - remaining;

          if (done > 0)
            {
              int i;

              for (i = 0; i < remaining; i++)
                {
                  * (pt->write_buf + i) = * (pt->write_buf + done + i);
                }
              pt->write_pos = pt->write_buf + remaining;
            }
#if HAVE_DECL_SCM_TERMINATING || defined (HAVE_LIBGUILE_TERMINATING)
          if (! scm_terminating)
            SYSTEM_ERROR ();
          else
#endif /* HAVE_DECL_SCM_TERMINATING || defined (HAVE_LIBGUILE_TERMINATING) */
            {
              const char *msg = "Error: could not"
                " flush large object file descriptor ";
              char buf[11];

              write (2, msg, strlen (msg));
              sprintf (buf, "%d\n", lobp->alod);
              write (2, buf, strlen (buf));

              count = remaining;
            }
        }
      ptr += count;
      remaining -= count;
    }
  pt->write_pos = pt->write_buf;
#undef FUNC_NAME
}

static void
lob_end_input (SCM port, int offset)
{
#define FUNC_NAME __func__
  scm_port *pt = SCM_PTAB_ENTRY (port);
  lob_stream *lobp = LOB_STREAM (port);
  PGconn *conn = LOB_CONN (lobp);
  int ret;

  /* Augment by the span of the read buffer.  */
  offset += pt->read_end - pt->read_pos;

  if (offset > 0)
    {
      pt->read_pos = pt->read_end;
      NOINTS ();
      ret = lo_lseek (conn, lobp->alod, -offset, SEEK_CUR);
      INTSOK ();
      if (PROB (ret))
        ERROR ("Error seeking on lo port ~S", port);
    }
  pt->rw_active = SCM_PORT_NEITHER;
#undef FUNC_NAME
}

static OFF_T
lob_seek (SCM port, OFF_T offset, int whence)
{
#define FUNC_NAME __func__
  lob_stream *lobp = LOB_STREAM (port);
  PGconn *conn = LOB_CONN (lobp);
  OFF_T ret;

  NOINTS ();
  ret = lo_lseek (conn, lobp->alod, offset, whence);
  INTSOK ();
  if (PROB (ret))
    ERROR ("Error (~S) seeking on lo port ~S", NUM_INT (ret), port);

  /* Adjust return value to account for guile port buffering.  */
  if (SEEK_CUR == whence)
    {
      scm_port *pt = SCM_PTAB_ENTRY (port);
      ret -= (pt->read_end - pt->read_pos);
    }

  return ret;
#undef FUNC_NAME
}

/* Fill a port's read-buffer with a single read.  Return the first char and
   move the ‘read_pos’ pointer past it, or return EOF if end of file.  */
static int
lob_fill_input (SCM port)
{
#define FUNC_NAME __func__
  scm_port *pt = SCM_PTAB_ENTRY (port);
  lob_stream *lobp = LOB_STREAM (port);
  PGconn *conn = LOB_CONN (lobp);
  int ret;

  if (pt->write_pos > pt->write_buf)
    lob_flush (port);

  NOINTS ();
  ret = lo_read (conn, lobp->alod, (char *) pt->read_buf, pt->read_buf_size);
  INTSOK ();
  if (PROB (ret))
    ERROR ("Error (~S) reading from lo port ~S", NUM_INT (ret), port);
  if (pt->read_buf_size && !ret)
    return EOF;
  pt->read_pos = pt->read_buf;
  pt->read_end = pt->read_buf + ret;

  return * (pt->read_buf);
#undef FUNC_NAME
}

static void
lob_write (SCM port, const void *data, size_t size)
{
#define FUNC_NAME __func__
  scm_port *pt = SCM_PTAB_ENTRY (port);

  if (pt->write_buf == &pt->shortbuf)
    {
      /* This is an "unbuffered" port.  */
      int fdes = SCM_FPORT_FDES (port);

      if (PROB (write (fdes, data, size)))
        SYSTEM_ERROR ();
    }
  else
    {
      const char *input = data;
      size_t remaining = size;

      while (remaining > 0)
        {
          size_t space = pt->write_end - pt->write_pos;
          size_t write_len = (remaining > space) ? space : remaining;

          memcpy (pt->write_pos, input, write_len);
          pt->write_pos += write_len;
          remaining -= write_len;
          input += write_len;
          if (write_len == space)
            lob_flush (port);
        }
      /* handle line buffering.  */
      if ((SCM_CELL_WORD_0 (port) & SCM_BUFLINE)
          && memchr (data, '\n', size))
        lob_flush (port);
    }
#undef FUNC_NAME
}

/* Check whether a port can supply input.  */
static int
lob_input_waiting_p (UNUSED SCM port)
{
  return 1;
}

static int
lob_close (SCM port)
{
  scm_port *pt = SCM_PTAB_ENTRY (port);
  lob_stream *lobp = LOB_STREAM (port);
  PGconn *dbconn = LOB_CONN (lobp);
  int ret;

  lob_flush (port);
  NOINTS ();
  ret = lo_close (dbconn, lobp->alod);
  INTSOK ();

  /* Free the buffers.  */
  if (pt->read_buf != &pt->shortbuf)
    free (pt->read_buf);
  if (pt->write_buf != &pt->shortbuf)
    free (pt->write_buf);

  return ret ? EOF : 0;
}

static SCM
lob_mark (SCM port)
{
  return DEFAULT_FALSE (SCM_OPENP (port),
                        LOB_STREAM (port)->conn);
}

static size_t
lob_free (SCM port)
{
  lob_stream *lobp = LOB_STREAM (port);

  if (SCM_OPENP (port))
    lob_close (port);
  GCFREE (lobp, lob_name);
  return GCRV (lobp);
}

static void
init_lob_support (void)
{
  lobp_tag = scm_make_port_type ("pg-lo-port", lob_fill_input, lob_write);
  scm_set_port_free          (lobp_tag, lob_free);
  scm_set_port_mark          (lobp_tag, lob_mark);
  scm_set_port_print         (lobp_tag, lob_printpt);
  scm_set_port_flush         (lobp_tag, lob_flush);
  scm_set_port_end_input     (lobp_tag, lob_end_input);
  scm_set_port_close         (lobp_tag, lob_close);
  scm_set_port_seek          (lobp_tag, lob_seek);
  scm_set_port_truncate      (lobp_tag, NULL);
  scm_set_port_input_waiting (lobp_tag, lob_input_waiting_p);
}

/* lob-legacy.c ends here */
